# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "heliosystem_accounts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "slug"
    t.integer "balance_cents", default: 0
    t.uuid "organization_id"
    t.uuid "parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slug"], name: "index_heliosystem_accounts_on_slug", unique: true
  end

  create_table "heliosystem_memberships", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "person_id"
    t.uuid "organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["organization_id"], name: "index_heliosystem_memberships_on_organization_id"
    t.index ["person_id"], name: "index_heliosystem_memberships_on_person_id"
  end

  create_table "heliosystem_organizations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "description"
    t.uuid "universe_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "color"
    t.index ["slug"], name: "index_heliosystem_organizations_on_slug", unique: true
    t.index ["universe_id"], name: "index_heliosystem_organizations_on_universe_id"
  end

  create_table "heliosystem_pages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "content"
    t.uuid "organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["organization_id", "slug"], name: "index_heliosystem_pages_on_organization_id_and_slug", unique: true
  end

  create_table "heliosystem_people", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "universe_id"
    t.uuid "user_id"
    t.boolean "admin", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["universe_id"], name: "index_heliosystem_people_on_universe_id"
    t.index ["user_id"], name: "index_heliosystem_people_on_user_id"
  end

  create_table "heliosystem_settings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.uuid "universe_id"
    t.uuid "organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["key"], name: "index_heliosystem_settings_on_key"
    t.index ["universe_id", "organization_id"], name: "index_heliosystem_settings_on_universe_id_and_organization_id"
    t.index ["universe_id"], name: "index_heliosystem_settings_on_universe_id"
  end

  create_table "heliosystem_subfield_values", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "subfield_id"
    t.string "app_type"
    t.uuid "app_id"
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["app_type", "app_id"], name: "index_heliosystem_subfield_values_on_app_type_and_app_id"
    t.index ["subfield_id", "app_type", "app_id"], name: "index_heliosystem_full_subfield_query"
  end

  create_table "heliosystem_subfields", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "label"
    t.string "format"
    t.string "app"
    t.uuid "universe_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["universe_id", "app", "label"], name: "index_heliosystem_subfields_on_universe_id_and_app_and_label"
    t.index ["universe_id", "app"], name: "index_heliosystem_subfields_on_universe_id_and_app"
    t.index ["universe_id", "label"], name: "index_heliosystem_subfields_on_universe_id_and_label"
  end

  create_table "heliosystem_subrelation_pairs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "subrelation_id"
    t.string "parent_type"
    t.uuid "parent_id"
    t.string "child_type"
    t.uuid "child_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subrelation_id", "child_type", "child_id"], name: "index_heliosystem_child_subrelation"
    t.index ["subrelation_id", "parent_type", "parent_id"], name: "index_heliosystem_parent_subrelation"
  end

  create_table "heliosystem_subrelations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "label"
    t.string "format"
    t.string "parent"
    t.string "child"
    t.uuid "universe_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["universe_id", "label"], name: "index_heliosystem_subrelations_on_universe_id_and_label"
    t.index ["universe_id", "parent", "label"], name: "index_heliosystem_subrelation_full_query"
    t.index ["universe_id", "parent"], name: "index_heliosystem_subrelations_on_universe_id_and_parent"
  end

  create_table "heliosystem_taggings", force: :cascade do |t|
    t.uuid "tag_id"
    t.string "taggable_type"
    t.uuid "taggable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tag_id"], name: "index_heliosystem_taggings_on_tag_id"
    t.index ["taggable_type", "taggable_id"], name: "index_heliosystem_taggings_on_taggable_type_and_taggable_id"
  end

  create_table "heliosystem_tags", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "description"
    t.string "color"
    t.uuid "universe_id"
    t.uuid "organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slug"], name: "index_heliosystem_tags_on_slug", unique: true
    t.index ["universe_id", "organization_id"], name: "index_heliosystem_tags_on_universe_id_and_organization_id"
    t.index ["universe_id"], name: "index_heliosystem_tags_on_universe_id"
  end

  create_table "heliosystem_timeclocks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.uuid "organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["organization_id", "slug"], name: "index_heliosystem_timeclocks_on_organization_id_and_slug", unique: true
    t.index ["slug"], name: "index_heliosystem_timeclocks_on_slug"
  end

  create_table "heliosystem_timepunches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "person_id"
    t.uuid "timeclock_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "heliosystem_universes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "slug"
    t.string "color"
    t.uuid "owner_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_heliosystem_universes_on_name", unique: true
    t.index ["slug"], name: "index_heliosystem_universes_on_slug", unique: true
  end

  create_table "heliosystem_users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "sysop", default: false
    t.string "first_name"
    t.string "last_name"
  end

end

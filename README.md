# HelioCenter

The bare-bones deployable wrapper for HelioSystem

## Getting Started

```
git clone --recurse-submodules -j4 https://gitlab.com/heliosystem/heliocenter.git
cd heliocenter
git submodule update --init --recursive

# and then make sure you have the latest updates to HelioSystem
cd engines/heliosystem
git pull origin master
```

We recommend running HelioCenter through Docker - you can be ready to roll with a `docker-compose build && docker-compose up`. See https://docs.docker.com/install/ and https://docs.docker.com/compose/install/ for help.
